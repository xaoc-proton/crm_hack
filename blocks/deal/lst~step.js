
exports.config = {
	multi: true,
}

exports.lst = ()=>{
	var lst = [
		{v: '', l: '\xa0'},
		{v: '1', l: 'Квалификация', perc: 1},
		{v: '2', l: 'Ценность для клиента', perc: 10},
		{v: '3', l: 'Коммерческое предложение', perc: 25},
		{v: '4', l: 'Решение', perc: 60},
		{v: '5', l: 'Контрактование', perc: 90},
		{v: '6', l: 'Закрытие сделки', perc: 100},
	];
}