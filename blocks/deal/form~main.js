
exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['deal']
}, callback);

exports.id = (__, code, callback)=>{
	__.fields[code].col = 'deal';
	__.fields[code].link = ['__deal'];
	__.queryIds[code] = (__.user.query && __.user.query.filter && __.user.query.filter.id) ? [__.user.query.filter.id] : [];
	callback();
}

exports.tpl = (_, d)=>{ return [

["div",{},[
		["div",{class: "card mb-3 mb-3"},[
			["div",{class: "card-header"},[
				["h5",{class: "mb-0"}]
			]],
			["div",{class: "card-body"},[
				["div",{class: "row mb-3"},[
					["div",{class: "col-4"},[
						["div",{role: "group",class: "input-group"},[
							["div",{class: "input-group-prepend"},[
								["div",{class: "input-group-text"},[
									["strong",{class: "text-danger"},[
										["span",{text: "!"}]
									]]
								]]
							]],
							["select",{class: "custom-select",id: "__BVID__13"},[
								["option",{value: ""},[
									["span",{text: "Все сделки"}]
								]],
								["option",{value: "a"},[
									["span",{text: "Не все сделки"}]
								]]
							]]
						]]
					]]
				]],
				["ol",{class: "breadcrumb"},[
					["li",{class: "breadcrumb-item"},[
						["a",{href: "#",target: "_self",class: ""},[
							["span",{text: "Организации"}]
						]]
					]],
					["li",{class: "breadcrumb-item active"},[
						["span",{"aria-current": "location"},[
							["span",{text: "Альянс Банк"}]
						]]
					]]
				]],
				["div",{class: "row bX1"},[
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "hash",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-hash b-icon bi"},[
									["g",{},[
										["path",{d: "M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "ID Сделки"}]
								]],
								["p",{},[
									["span",{text: "251328"}]
								]]
							]]
						]]
					]],
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "hash",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-hash b-icon bi"},[
									["g",{},[
										["path",{d: "M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "Ожидаемая выручка с ндс"}]
								]],
								["p",{},[
									["span",{text: "15 000 000 ₽"}]
								]]
							]]
						]]
					]],
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "hash",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-hash b-icon bi"},[
									["g",{},[
										["path",{d: "M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "Ожидаемая выручка без ндс"}]
								]],
								["p",{},[
									["span",{text: "12 000 000 ₽"}]
								]]
							]]
						]]
					]],
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "hash",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-hash b-icon bi"},[
									["g",{},[
										["path",{d: "M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "статус"}]
								]],
								["p",{},[
									["span",{text: "Открыта"}]
								]]
							]]
						]]
					]],
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "person",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-person b-icon bi"},[
									["g",{},[
										["path",{d: "M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "Ответственный"}]
								]],
								["p",{},[
									["span",{text: "Смирнов И.С."}]
								]]
							]]
						]]
					]],
					["div",{class: "col-auto mb-2"},[
						["div",{class: "media"},[
							["div",{class: "media-aside align-self-start"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "hash",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-hash b-icon bi"},[
									["g",{},[
										["path",{d: "M8.39 12.648a1.32 1.32 0 0 0-.015.18c0 .305.21.508.5.508.266 0 .492-.172.555-.477l.554-2.703h1.204c.421 0 .617-.234.617-.547 0-.312-.188-.53-.617-.53h-.985l.516-2.524h1.265c.43 0 .618-.227.618-.547 0-.313-.188-.524-.618-.524h-1.046l.476-2.304a1.06 1.06 0 0 0 .016-.164.51.51 0 0 0-.516-.516.54.54 0 0 0-.539.43l-.523 2.554H7.617l.477-2.304c.008-.04.015-.118.015-.164a.512.512 0 0 0-.523-.516.539.539 0 0 0-.531.43L6.53 5.484H5.414c-.43 0-.617.22-.617.532 0 .312.187.539.617.539h.906l-.515 2.523H4.609c-.421 0-.609.219-.609.531 0 .313.188.547.61.547h.976l-.516 2.492c-.008.04-.015.125-.015.18 0 .305.21.508.5.508.265 0 .492-.172.554-.477l.555-2.703h2.242l-.515 2.492zm-1-6.109h2.266l-.515 2.563H6.859l.532-2.563z"}]
									]]
								]]
							]],
							["div",{class: "media-body"},[
								["h5",{class: "mt-0"},[
									["span",{text: "war"}]
								]],
								["p",{},[
									["span",{text: "Да"}]
								]]
							]]
						]]
					]]
				]]
			]]
		]],
		["div",{class: "card mb-3 no-header card-steps"},[
			["div",{class: "card-header"},[
				["h5",{class: "mb-0"}]
			]],
			["div",{class: "card-body"},[
				["div",{class: "row"},[
					["div",{class: "col-auto"},[
						["button",{type: "button",class: "btn btn-outline"},[
							["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "arrow down short",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-arrow-down-short b-icon bi"},[
								["g",{},[
									["path",{"fill-rule": "evenodd",d: "M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4z"}]
								]]
							]]
						]]
					]],
					["div",{class: "col step1"},[
						["div",{class: "arrow-block"},[
							["span",{text: "Квалификация"}],
							["small",{},[
								["span",{text: "1%"}]
							]]
						]]
					]],
					["div",{class: "col step2"},[
						["div",{class: "arrow-block"},[
							["span",{text: "Ценность для клиента"}],
							["small",{},[
								["span",{text: "10%"}]
							]]
						]]
					]],
					["div",{class: "col step3"},[
						["div",{class: "arrow-block"},[
							["span",{text: "Коммерческое предложение"}],
							["small",{},[
								["span",{text: "25%"}]
							]]
						]]
					]],
					["div",{class: "col step4"},[
						["div",{class: "arrow-block primary"},[
							["span",{text: "Решение"}],
							["small",{},[
								["span",{text: "60%"}]
							]]
						]]
					]],
					["div",{class: "col"},[
						["div",{class: "arrow-block outline"},[
							["span",{text: "Контрактование"}],
							["small",{},[
								["span",{text: "90%"}]
							]]
						]]
					]],
					["div",{class: "col"},[
						["div",{class: "arrow-block outline"},[
							["span",{text: "Закрытие сделки"}],
							["small",{},[
								["span",{text: "100%"}]
							]]
						]]
					]],
					["div",{class: "col"},[
						["button",{type: "button",class: "btn text-nowrap btn-primary arrow-block-btn"},[
							["span",{text: "сделать текущим этапом"}]
						]]
					]]
				]]
			]]
		]],
		["div",{class: "row"},[
			["div",{class: "col-7"},[
				["div",{class: "card mb-3 mb-3 card-info"},[
					["div",{class: "card-header"},[
						["h5",{class: "mb-0"},[
							["span",{text: "Информация"}]
						]]
					]],
					["div",{class: "card-body"},[
						["div",{class: "row mb-3"},[
							["div",{class: "col-4"},[
								["fieldset",{class: "form-group",id: "__BVID__17"},[
									["legend",{tabindex: "-1",class: "bv-no-focus-ring col-form-label pt-0",id: "__BVID__17__BV_label_"},[
										["span",{text: "Enter your name"}]
									]],
									["div",{},[
										["input",{type: "text",class: "form-control",id: "__BVID__18"}]
									]]
								]]
							]],
							["div",{class: "col-4"},[
								["fieldset",{class: "form-group",id: "__BVID__19"},[
									["legend",{tabindex: "-1",class: "bv-no-focus-ring col-form-label pt-0",id: "__BVID__19__BV_label_"},[
										["span",{text: "Enter your name"}]
									]],
									["div",{},[
										["select",{class: "custom-select",id: "__BVID__20"},[
											["option",{value: ""},[
												["span",{text: "Все сделки"}]
											]],
											["option",{value: "a"},[
												["span",{text: "Не все сделки"}]
											]]
										]]
									]]
								]]
							]],
							["div",{class: "col-4"},[
								["fieldset",{class: "form-group",id: "__BVID__21"},[
									["legend",{tabindex: "-1",class: "bv-no-focus-ring col-form-label pt-0",id: "__BVID__21__BV_label_"},[
										["span",{text: "Enter your name"}]
									]],
									["div",{},[
										["select",{class: "custom-select",id: "__BVID__22"},[
											["option",{value: ""},[
												["span",{text: "Все сделки"}]
											]],
											["option",{value: "a"},[
												["span",{text: "Не все сделки"}]
											]]
										]]
									]]
								]]
							]]
						]],
						["div",{class: "row mb-3"},[
							["div",{class: "col-4"},[
								["select",{class: "custom-select",id: "__BVID__23"},[
									["option",{value: ""},[
										["span",{text: "Все сделки"}]
									]],
									["option",{value: "a"},[
										["span",{text: "Не все сделки"}]
									]]
								]]
							]],
							["div",{class: "col-4"},[
								["input",{type: "text",class: "form-control",id: "__BVID__24"}]
							]],
							["div",{class: "col-4"},[
								["select",{class: "custom-select",id: "__BVID__25"},[
									["option",{value: ""},[
										["span",{text: "Все сделки"}]
									]],
									["option",{value: "a"},[
										["span",{text: "Не все сделки"}]
									]]
								]]
							]]
						]],
						["div",{class: "row mb-3"},[
							["div",{class: "col-4"},[
								["select",{class: "custom-select",id: "__BVID__26"},[
									["option",{value: ""},[
										["span",{text: "Все сделки"}]
									]],
									["option",{value: "a"},[
										["span",{text: "Не все сделки"}]
									]]
								]]
							]],
							["div",{class: "col-4"},[
								["input",{type: "text",class: "form-control",id: "__BVID__27"}]
							]],
							["div",{class: "col-4"},[
								["input",{type: "text",class: "form-control",id: "__BVID__28"}]
							]]
						]],
						["div",{class: "row mb-3"},[
							["div",{class: "col-4"},[
								["input",{type: "text",class: "form-control",id: "__BVID__29"}]
							]],
							["div",{class: "col-4"},[
								["input",{type: "text",class: "form-control",id: "__BVID__30"}]
							]],
							["div",{class: "col-4"},[
								["div",{class: "custom-control custom-switch b-custom-control-sm"},[
									["input",{type: "checkbox",class: "custom-control-input",value: "true",id: "__BVID__31"}],
									["label",{class: "custom-control-label",for: "__BVID__31"},[
										["span",{text: "Да"}]
									]]
								]]
							]]
						]],
						["fieldset",{class: "form-group form-bages mb-3",id: "__BVID__32"},[
							["legend",{tabindex: "-1",class: "bv-no-focus-ring col-form-label pt-0",id: "__BVID__32__BV_label_"},[
								["span",{text: "БЮ"}]
							]],
							["div",{},[
								["span",{class: "badge badge-secondary"},[
									["span",{text: "T1-Integration"}]
								]],
								["span",{class: "badge badge-secondary"},[
									["span",{text: "T1-Cloud"}]
								]],
								["span",{class: "badge badge-secondary"},[
									["span",{text: "T1-Consulting"}]
								]]
							]]
						]],
						["p",{},[
							["button",{type: "button",class: "test btn btn-secondary"+`css
								position: relative;
							`},[
								["span",{text: "Поиск сотрудника"}]
							]]
						]],
						["div",{class: "card collapse mb-3 mb-3"},[
							["div",{class: "card-header"},[
								["h5",{class: "mb-0"},[
									["span",{text: "Тендер / Конкурс"}]
								]]
							]],
							["div",{class: "card-body"},[
								["span",{text: "123"}]
							]]
						]]
					]]
				]],
				["div",{class: "card mb-3 mb-3"},[
					["div",{class: "card-header"},[
						["h5",{class: "mb-0"},[
							["span",{text: "Продукты сделки"}]
						]]
					]],
					["div",{class: "card-body"}]
				]],
				["div",{class: "card mb-3 mb-3"},[
					["div",{class: "card-header"},[
						["h5",{class: "mb-0"},[
							["span",{text: "Контактные лица"}]
						]]
					]],
					["div",{class: "card-body"}]
				]]
			]],
			["div",{class: "col-5"},[
				["div",{class: "card mb-3 card-team"},[
					["div",{class: "card-header"},[
						["div",{class: "row"},[
							["div",{class: "col"},[
								["h5",{class: "mb-0"},[
									["span",{text: "Команда"}]
								]]
							]],
							["div",{class: "col-auto ml-auto mr-n2"},[
								["button",{type: "button",class: "btn btn-link btn-sm"},[
									["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "plus",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-plus b-icon bi"},[
										["g",{},[
											["path",{d: "M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"}]
										]]
									]],
									["span",{text: "Добавить"}]
								]],
								["button",{type: "button",class: "btn btn-link btn-sm"},[
									["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "search",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-search b-icon bi"},[
										["g",{},[
											["path",{d: "M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"}]
										]]
									]]
								]],
								["button",{type: "button",class: "btn btn-link btn-sm"},[
									["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "three dots",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-three-dots b-icon bi"},[
										["g",{},[
											["path",{d: "M3 9.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"}]
										]]
									]]
								]]
							]]
						]]
					]],
					["div",{class: "card-body pt-0"},[
						["div",{class: "main-table table-responsive"},[
							["table",{role: "table",class: "table b-table table-hover table-sm",id: "__BVID__36"},[
								["thead",{role: "rowgroup",class: ""},[
									["tr",{role: "row",class: ""},[
										["th",{role: "columnheader",scope: "col",class: "min-width"}],
										["th",{role: "columnheader",scope: "col",class: ""},[
											["span",{text: "ФИО"}]
										]],
										["th",{role: "columnheader",scope: "col",class: ""},[
											["span",{text: "Роль в сделке"}]
										]],
										["th",{role: "columnheader",scope: "col",class: ""},[
											["span",{text: "Телефон"}]
										]]
									]]
								]],
								["tbody",{role: "rowgroup"},[
									["tr",{role: "row",class: ""},[
										["td",{role: "cell",class: "min-width"},[
											["div",{class: "custom-control custom-checkbox b-custom-control-sm"},[
												["input",{type: "checkbox",class: "custom-control-input",value: "true",id: "__BVID__46"}],
												["label",{class: "custom-control-label",for: "__BVID__46"}]
											]]
										]],
										["td",{role: "cell",class: ""},[
											["span",{text: "Смирнов Олег Иванович"}]
										]],
										["td",{role: "cell",class: ""},[
											["span",{class: "badge badge-light"},[
												["span",{text: "ЛПР"}]
											]]
										]],
										["td",{role: "cell",class: ""},[
											["span",{text: "+7 (915) 612 34 72"}]
										]]
									]]
								]]
							]]
						]]
					]]
				]],
				["div",{class: "card mb-3 card-comments"},[
					["div",{class: "card-header"},[
						["div",{class: "row"},[
							["div",{class: "col"},[
								["h5",{class: "mb-0"},[
									["span",{text: "Заметки и вложения"}]
								]]
							]],
							["div",{class: "col-auto ml-auto mr-n2"},[
								["button",{type: "button",class: "btn btn-link btn-sm"},[
									["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "plus",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-plus b-icon bi"},[
										["g",{},[
											["path",{d: "M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"}]
										]]
									]],
									["span",{text: "Добавить"}]
								]]
							]]
						]]
					]],
					["div",{class: "card-body pt-0"},[
						["div",{class: "bBox"},[
							["div",{class: "float-right"},[
								["span",{text: "25.05.21"}]
							]],
							["h4",{},[
								["span",{text: "Порошин Т. Н."}]
							]],
							["p",{},[
								["span",{text: "В документе расписан бюджет на текущий этап. Прошу изучить его и подготовить отчет согласно новой статистике"}]
							]]
						]],
						["div",{class: "bBox"},[
							["div",{class: "float-right"},[
								["span",{text: "25.05.21"}]
							]],
							["h4",{},[
								["span",{text: "Порошин Т. Н."}]
							]],
							["p",{},[
								["span",{text: "В документе расписан бюджет на текущий этап. Прошу изучить его и подготовить отчет согласно новой статистике"}]
							]]
						]]
					]]
				]],
				["div",{class: "card mb-3 card-tasks"},[
					["div",{class: "card-header"},[
						["div",{class: "row"},[
							["div",{class: "col"},[
								["h5",{class: "mb-0"},[
									["span",{text: "Задачи"}]
								]]
							]],
							["div",{class: "col-auto ml-auto mr-n2"},[
								["button",{type: "button",class: "btn btn-link btn-sm"},[
									["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "plus",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-plus b-icon bi"},[
										["g",{},[
											["path",{d: "M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"}]
										]]
									]],
									["span",{text: "Создать задачу"}]
								]]
							]]
						]],
						["hr",{class: "mt-0 mb-3"}],
						["ul",{class: "nav nav-pills"},[
							["li",{class: "nav-item"},[
								["a",{href: "#",target: "_self",class: "nav-link active"},[
									["span",{text: "Новые"}]
								]]
							]],
							["li",{class: "nav-item"},[
								["a",{href: "#",target: "_self",class: "nav-link"},[
									["span",{text: "В работе"}]
								]]
							]],
							["li",{class: "nav-item"},[
								["a",{href: "#",target: "_self",class: "nav-link"},[
									["span",{text: "Завершены"}]
								]]
							]],
							["li",{class: "nav-item"},[
								["a",{href: "#",target: "_self",class: "nav-link"},[
									["span",{text: "Отменены"}]
								]]
							]]
						]]
					]],
					["div",{class: "card-body -pt-0 gray"},[
						["div",{class: "bTask danger"},[
							["h4",{},[
								["span",{text: "Звонок"}]
							]],
							["p",{},[
								["span",{text: "Договориться о встрече"}]
							]],
							["p",{},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "clock",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-clock b-icon bi"},[
									["g",{},[
										["path",{d: "M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"}],
										["path",{d: "M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"}]
									]]
								]],
								["span",{text: "31.07.21 в 15:30"}]
							]]
						]],
						["div",{class: "bTask warning"},[
							["h4",{},[
								["span",{text: "Звонок"}]
							]],
							["p",{},[
								["span",{text: "Договориться о встрече"}]
							]],
							["p",{},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "clock",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-clock b-icon bi"},[
									["g",{},[
										["path",{d: "M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"}],
										["path",{d: "M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"}]
									]]
								]],
								["span",{text: "31.07.21 в 15:30"}]
							]]
						]],
						["div",{class: "bTask success"},[
							["h4",{},[
								["span",{text: "Звонок"}]
							]],
							["p",{},[
								["span",{text: "Договориться о встрече"}]
							]],
							["p",{},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "clock",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-clock b-icon bi"},[
									["g",{},[
										["path",{d: "M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"}],
										["path",{d: "M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"}]
									]]
								]],
								["span",{text: "31.07.21 в 15:30"}]
							]]
						]]
					]]
				]]
			]]
		]]
	]]

]}

exports.func = ()=>{

}

exports.script = ()=>{

	window.updateMenuCustom = function (info)
	{
		if (!window.breadcrumb.length)
			window.breadcrumb.push({text: "Юридические лица", query: { form: "ce~list", container: "formContent" } });

		let $nameField = $("#formContent .el-name:not(.no-breadcrumb) .el-value");
		
		window.breadcrumb.push({
			text: $nameField.text() || $nameField.val(),
			query: history.state.subform, active: true,
		});
	};
}