
exports.config = {
	menu: {label: 'Сделки', icon: 'fas fa-atlas'},
}

exports.access = (__, data, callback)=>SYS.requireFile('func', 'user~access').f(__, {
	allow: ['deal']
}, callback);

exports.id = (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.fields[code].link = ['__'+__.fields[code].col];
	__.queryIds[code] = [__.user.key];
	callback();
}

exports.tpl = (_, d)=>{ return [

	_.html('__tpl~list', _, d, {
		content: (_, d)=>[
		
			_.html('__tpl~table', _, d, {
				col: 'deal', 
				label: 'Сделка',
				filter: {
					findBtn: {
						class: 'col-7'
					},
					items: [
						{class: "col-3", f: {name: 'status', label: 'Сделки', type: 'select', lst: 'deal~status', value: ''}},
					],
				},
				add: {
					modal: true,
					items: [
						{f: {name: 'name', 		label: "Название"}},
						{f: {name: 'step', 		label: "Этап", type: 'select', lst: 'deal~step'}},
						{f: {name: 'client', 	label: "Компания", type: 'select2', lst: 'ce', ajax: true}},
						{f: {name: 'date', 		label: "Дата подписания договора", type: 'date'}},
						{f: {name: 'user', 		label: "Ответственный", type: 'select', lst: 'user', ajax: true}},
						{f: {name: 'status', 	label: "Статус сделки", type: 'select', lst: 'deal~status'}},
						{f: {name: 'war', 		label: "WAR", type: 'check'}},
						{f: {name: 'money', 	label: "Ожидаемая выручка с НДС", type: 'money'}},
					],
					required: [
						{f: 'name', err: 'Название должно быть указано'},
					],
					afterValid: async function()
					{
					},
					beforeAdd: async function()
					{
						const tmp_obj = this.dataFind({col: 'tmp_obj'});
						tmp_obj.add_time = Date.now();
					},
				},
				table: {
					items: [
						//{label: 'Добавлена', f: {name: 'add_time', type: 'datetime--', value: ''}},
						{label: 'Название сделки', f: {name: 'name', type: 'text--', value: ''}},
						{label: "Этап", f: {name: 'step', type: 'select--', lst: 'deal~step'}},
						{label: "Компания", f: {name: 'client', type: 'select--', lst: 'ce', ajax: true}},
						{label: "Дата подписания договора", f: {name: 'date', type: 'date--'}},
						{label: "Ответственный", f: {name: 'user', type: 'select--', lst: 'user', ajax: true}},
						{label: "Статус сделки", f: {name: 'status', type: 'select--', lst: 'deal~status'}},
						{label: "WAR", f: {name: 'war', type: 'check--'}},
						{label: "Ожидаемая выручка с НДС", f: {name: 'money', type: 'money--'}},
					],
					id: {
						baseField: 'add_time',
						prepare: (select)=>{
							
							if(select.filter.find_text){
								if(!SYS.isNum(select.filter.find_text)){
									select.where.push("deal__name.f LIKE ?");
									select.escape.push('%'+select.filter.find_text.trim()+'%');
								}else{
									// select.join.push("LEFT JOIN ce__inn ON ce__inn.id = ce__name.id");
									// select.where.push("ce__name.f LIKE ? OR ce__inn.f LIKE ?");
									// select.escape.push('%'+select.filter.find_text.trim()+'%', '%'+select.filter.find_text.trim()+'%');
								}
							}
							
							select.order = ['deal__add_time.f'];
						},
					},
				}
			}),
		]
	}),
]}

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}