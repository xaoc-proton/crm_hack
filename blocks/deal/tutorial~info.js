
exports.config = {
	multi: true,
	sfx: true,
}

exports.lst = ()=>{

	var t = 10;

	var lst = [{
		v: 'info', l: 'info',
		action: function(callback){
			waitForLoadElementByCode[$('#formContent').attr('code')] = function(){ callback() };
			$('#side-menu li[form="deal~list"] > a').trigger('click');
		},
		text: [{
			t: 'Это список всех сделок в системе',
			controls: {
				'Продолжай': function(){ window.setTutorialComplete({storageQuery: {form: 'deal~list', container:"formContent"}}) },
			},
		}],
		position: 'bottomRight',
	},{
		v: 'filter', l: 'filter',
		check: {search: '#formContent .el-status.filter-field', fail: {form:'#formContent'}},
		text: [{
			active: {l: '#formContent .el-status.filter-field', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Этот фильтр позволяет сортировать таблицу по статусам сделок.',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	},{
		v: 'deal_list', l: 'deal_list',
		check: {search: '#deal-table', fail: {form:'#formContent', err: {nextStep: 'end'}}},
		action: function(callback){
			// обновляем статус tutorial только после обновления формы, чтобы не было ошибок асинхрона вызовов*
			// * если по честноку, то я так и не разобрался, почему вылазит ошибка вызова setTutorial с пустой data
			waitForLoadElementByCode[$('#formContent').attr('code')] = function(){
				window.setTutorialComplete({storageQuery: $(this).attr('query')});
			}
			callback();
		},
		text: [{
			t:'Выбери любую сделку для просмотра.',
			controls: {
				'Нажми на любую строку в таблице сделок': false,
				exit: {t: 'Не нужно, все понятно', f: function(){
					// !!! тут нужно сбрасывать обработчик из action
					window.setTutorialComplete({endTutorial: true});
				}},
			},
		}],
		position: 'bottomRight',
	},{
		v: 'form', l: 'form',
		text: [{
			t: 'Это форма сделки',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	},
	{
		v: 'form_info', l: 'form_info',
		check: {search: '#formContent .card-info', fail: {form:'#formContent'}},
		text: [{
			active: '#formContent .card-info',
			t: 'Это форма для редактирования информации о сделке.',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	},
	{
		v: 'form_team', l: 'form_team',
		check: {search: '#formContent .card-team', fail: {form:'#formContent'}},
		text: [{
			active: '#formContent .card-team',
			t: 'Это команда.',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomLeft',
	},
	{
		v: 'form_comments', l: 'form_comments',
		check: {search: '#formContent .card-comments', fail: {form:'#formContent'}},
		text: [{
			active: '#formContent .card-comments',
			t: 'Это заметки и вложения.',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomLeft',
	},
	{
		v: 'form_tasks', l: 'form_tasks',
		check: {search: '#formContent .card-tasks', fail: {form:'#formContent'}},
		text: [{
			active: '#formContent .card-tasks',
			t: 'Это задачи.',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomLeft',
	},
	{
		v: 'return', l: 'return',
		action: function(callback){
			waitForLoadElementByCode[$('#formContent').attr('code')] = function(){ callback() };
			$('#side-menu li[form="deal~list"] > a').trigger('click');
		},
		text: [{
			t: 'Мы вернулись назад, чтобы продолжить обучение',
			controls: {
				'Продолжай': function(){ window.setTutorialComplete({storageQuery: {form: 'deal~list', container:"formContent"}}) },
			},
		}],
		position: 'bottomRight',
	},{
		v: 'end', l: 'end',
		text: [{
			t: 'Если что-то непонятно в карточке сделки, то нажимай на мои иконки, для получения подсказки.',
			controls: {
				exit: {t: 'Понятно, спасибо', f: function(){ window.setTutorialComplete({endTutorial: true}) }},
			},
		}],
		position: 'bottomRight',
	}];
}

exports.func = {
	f: function(conn, data, callback){try
	{
		if(data.msg.nextStep)
		{
			const update = [{n: 'tutorial.active', v: {t: conn.user.tutorial.active.t, s: data.msg.nextStep}}];
			conn.user.update(conn, Object.assign(data, {msg: update}));
		}
		
		callback({status: 'ok'});
		
	}catch(err){ callback({status: 'err', err: 'Ошибка перехода на следующий шаг обучения'}) }}
}

exports.action = {
	system: true,
	prepareUser: true,
	customGlobal: true,
	f: function(conn, data, callback){try{
		
		var w = [], index = 'deal~tutorial_info', version = 4;

		if(data.prepareUser){
			
			if((conn.user.config.prepare[index]||0)*1 < version) w.push((cb)=>{try{

				conn.user.update(conn, Object.assign(data, {msg: [
					{n: 'config.prepare.'+index, v: version},
					{n: 'tutorial.links.'+index+'.link', v: '#side-menu li[form="deal~list"]'},
					{n: 'tutorial.links.'+index+'.label', v: 'Сделки'},
					{n: 'tutorial.links.'+index+'.status', type: 'unset'},
				]}));
				
				cb();
				
			}catch(e){ console.log(e); cb() }});

			async.waterfall(w, ()=>{
				
				callback({status: 'ok'});
			
			});
		}else{
			
			async.waterfall([(cb)=>{
				
				/*conn.db.collection('__tutorial').findOne({}, {__fake_worker: 1}, (err, tutorial)=>{
					
					if(!tutorial.__fake_worker){
						
						DB.addComplex(conn, {col: 'pp'}, [], {
							add_time: -1, first_name: 'Тест', second_name: 'Тестовый', third_name: 'Тестович',
						}, (pp)=>{
							
							DB.addComplex(conn, {col: 'worker', links: {
								worker: {pp: '__pp', __tutorial: '____tutorial'},
								pp: '__worker',
								__tutorial: '__fake_worker',
							}}, [
								{col: 'pp', _id: pp._id},
								{col: '__tutorial', _id: tutorial._id},
							], {
								add_time: -1, code: '999999', position: 'Фейковый сотрудник для обучения',
							}, ()=>{
								
								DB.addComplex(conn, {col: 'user'}, [{col: 'pp', _id: pp._id}], {
									add_time: -1,
								}, ()=>{
									cb();
								});
							});
						});
					}else{ cb() }
				})*/
				cb();
			}], ()=>{
				callback({status: 'ok'});
			});
		
		}
	}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка'}) }}
}