
exports.config = {
	multi: true,
	sfx: true,
}

exports.lst = ()=>{

	var t = 10;

	var lst = [{
		v: 'btn', l: 'btn',
		check: {search: '#formContent button.test', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent button.test', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Это кнопка. Ее можно нажимать.',
			controls: {
				'Спасибо': function(){ window.setTutorialComplete({endTutorial: true}) },
			},
		}],
		position: 'bottomRight',
	}];
}

/*exports.func = {
	// вызывается в самом конце tutorial~status
	f: function(conn, data, callback){
		callback();
	}
}*/

exports.action = {
	system: true,
	prepareUser: true,
	//customGlobal: true, // если понадобится создавать фейковые сущности
	f: async function(conn, data, callback){
		
		const index = 'deal~tutorial_btn', version = 7;

		if(data.prepareUser)
		{
			if((conn.user.config.prepare[index]||0)*1 < version)
			{
				conn.user.update(conn, Object.assign(data, {msg: [
					{n: 'config.prepare.'+index, v: version},
					{n: 'tutorial.links.'+index+'.link', v: '#formContent button.test'},
					{n: 'tutorial.links.'+index+'.label', v: 'Учимся нажимать кнопку'},
					{n: 'tutorial.links.'+index+'.status', type: 'unset'},
				]}));
			}
				
			callback( OK() );

		}else{
			callback( OK() );
		}
	}
}