
exports.config = {
	multi: true,
	sfx: true,
}

exports.lst = ()=>{

	var t = 50;

	var lst = [{
		v: 'list', l: 'list',
		check: {search: '#formContent .card-steps', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps'},
			t: 'Это этапы сделки.',
			controls: {
				'Спасибо': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	},{
		v: 'btn', l: 'btn',
		check: {search: '#formContent .card-steps .btn-primary', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps .btn-primary'},
			t: 'Эта кнопка что-то делает...',
			controls: {
				'Я понял': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomLeft',
	},{
		v: 'step1', l: 'step1',
		check: {search: '#formContent .card-steps .step1', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps .step1', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Этот этап означает что...',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
				'Э1': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'topRight',
	},{
		v: 'step2', l: 'step2',
		check: {search: '#formContent .card-steps .step2', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps .step2', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Этот этап означает что...',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
				'Э1': function(){ window.setTutorialComplete() },
				'Э2': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'topRight',
	},{
		v: 'step3', l: 'step3',
		check: {search: '#formContent .card-steps .step3', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps .step3', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Этот этап означает что...',
			controls: {
				'Дальше': function(){ window.setTutorialComplete() },
				'Э1': function(){ window.setTutorialComplete() },
				'Э2': function(){ window.setTutorialComplete() },
				'Э3': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'topRight',
	},{
		v: 'step4', l: 'step4',
		check: {search: '#formContent .card-steps .step4', fail: {form:'#formContent', query: {form: 'deal~main', container:"formContent"}}},
		text: [{
			active: {l: '#formContent .card-steps .step4', class: 'shadow-not-bootstrap', noBodyAttr: true},
			t: 'Это последний этап, про который я хоть что-то знаю, извини.',
			controls: {
				'Ясно, спасибо за все': function(){ window.setTutorialComplete({endTutorial: true}) },
			},
		}],
		position: 'topLeft',
	}];
}

/*exports.func = {
	// вызывается в самом конце tutorial~status
	f: function(conn, data, callback){
		callback();
	}
}*/

exports.action = {
	system: true,
	prepareUser: true,
	//customGlobal: true, // если понадобится создавать фейковые сущности
	f: async function(conn, data, callback){
		
		const index = 'deal~tutorial_steps', version = 3;

		if(data.prepareUser)
		{
			if((conn.user.config.prepare[index]||0)*1 < version)
			{
				conn.user.update(conn, Object.assign(data, {msg: [
					{n: 'config.prepare.'+index, v: version},
					{n: 'tutorial.links.'+index+'.link', v: '#formContent .card-steps'},
					{n: 'tutorial.links.'+index+'.label', v: 'Этапы сделки'},
					{n: 'tutorial.links.'+index+'.status', type: 'unset'},
				]}));
			}
				
			callback( OK() );

		}else{
			callback( OK() );
		}
	}
}