
exports.config = {
	multi: true,
	customName: true,
}

exports['modal'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{
	
		if(!c.add.items) c.add.items = [];
	
	return [

		["div",{"class": "modal fade tpl-theme-modal","role": "dialog","id": c.col+"-modal"},[
			c.add.modal.html ? c.add.modal.html(_, d) : [
				_.if(_.editMode, ()=>[
					_.c({name: 'tmp_obj_'+c.col, col: 'tmp_obj', config: c, process: {
						tpl: (_, d)=>{
						
							if(_.__.pre){
								Object.keys(_.config.add||[]).forEach((key)=>{
									if(typeof _.config.add[key] == 'function'){
										_.__.process[_.linecode+'_'+key] = {action: _.config.add[key]};
									}
								});
							}
						
							_.config.add.items.filter(_=>_).forEach((item, key)=>{
								if(_.__.pre){
									if(item.html){
										_.__.process[_.linecode+'_add_item_'+key] = {tpl: item.html};
									}
								}else{
									if(_.__.process[_.linecode+'_add_item_'+key]) item.html = _.__.process[_.linecode+'_add_item_'+key].tpl;
								}
							});
							
						return [
							
							["div",{"class": "modal-dialog","role": "document"},[
								
								["div",{"class": "modal-content"},[
									
									["div",{"class": "modal-header"},[
										["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
											["span",{"aria-hidden": "true"},[
												["i",{"class": "fas fa-times"}]
											]]
										]],
										["h4",{"class": "modal-title"},[
											["span", {"text": "Новый "+(_.config.label||'')}],
										]]
									]],
									
									["div",{"class": "modal-body"},[
										["div",{"role": "form", class: 'm-t'},[
										
											_.config.add.items.filter(_=>_).map(item=>[]
												.concat(item.f ? [_.f( item.f )] : [])
												.concat(item.html ? item.html(_, d) : [])
											),
											
										]],
									]],
									
									["div",{"class": "modal-footer"},[
										["button",{"type": "button","class": "btn btn-default","data-dismiss": "modal"},[
											["span",{"text": "Отменить"}]
										]],
										["button",{"type": "button","class": "btn btn-primary"},[
											["span",{"text": "Создать"}],
											_.f({name: '__tpl~modalAdd', type: 'action', process: (()=>{
												var obj = {};
												(SYS.get(_, 'config.add.required')||[]).forEach((item, key)=>{
													if(typeof item == 'function') obj['required_'+key] = item;
												});
												return obj;
											})(),front: {
												onAction: ()=>location.reload(),
												onError: ($e)=>reloadComplex($e),
											}}),
										]]
									]],
								]],
							]],
						]},
					}}),
				]),
			],	
		]],
		
	]},
	script: ()=>{
		$('.tpl-theme-modal').on('shown.bs.modal', function(e){
			if(window.resizeAllTextarea) window.resizeAllTextarea($(e.target));
		});
	},
	 style: ()=>{/*
		.tpl-theme-modal {
			position: fixed!important;
		}
		.tpl-theme-modal .el > .select2 {
			min-width: 100%;
		}
	*/},
}

exports["modalAdd"] = {
	config: {
		customType: "action",
		customName: key=>key, // поиск функции идет без '.js', который по дефолту добавляется в SYS.updateBLOCK
	},
	f: async function (msg) {
		
		const config = this.actionField.parent.config;
		const tmp_obj = this.dataFind({col: 'tmp_obj'});
		
		const check = (SYS.get(config, "add.required") || [])
						.map((item, key) =>
							item || this.form.process[this.actionField.parent.linecode][`required_${key}__${this.actionField.field.name}`].call(this)
						)
						.filter(item =>
							!( item != undefined && !item.err
								? item 
								: SYS.get(tmp_obj, item.f + (typeof tmp_obj[item.f] == "object" ? ".v" : "")) )
						);
		
		if(check.length)
			return {status: "err", err: check.map(_ => _.err).join("; ")};
		
		const afterValid = SYS.get(this.form.process[this.actionField.parent.linecode + "_afterValid"], "action");
		const beforeAdd = SYS.get(this.form.process[this.actionField.parent.linecode + "_beforeAdd"], "action");
		const afterAdd = SYS.get(this.form.process[this.actionField.parent.linecode + "_afterAdd"], "action");
		
	// ---
		if(typeof afterValid == 'function') afterValid.call(this); // все что после валидации (в т.ч. дополнительная валидация) кладем сюда
		
		// удаление tmp_obj из родителя (чтобы автоматом создался новый)
		this.user['__tmp_obj_'+config.col].l = [];
	//---
		
		// автоматический прирост глобального счетчика
		const __content = await this.findOne('__content', DB.__content._id);
		const counter = (SYS.get(__content, `counters.${config.col}`)||0)*1;
		SYS.set(__content, `counters.${config.col}`, counter+1);
		
		tmp_obj.code = counter + 1; // автоматически присваиваем код объекта (для комплексных кодов делаем переопределение в beforeAdd)
		delete tmp_obj.add_time;
		delete tmp_obj._id;
		delete tmp_obj.col;
		
		if(typeof beforeAdd == 'function') beforeAdd.call(this); // кладем сюда все что до добавления объекта (кроме валидации)
		
		// "красивая" подмена объекта (saveTransaction распознает его как новый)
		tmp_obj._meta.col = config.col;
		tmp_obj._meta._id = ObjectId();
		tmp_obj._meta.insert = true;
		
		if(typeof afterAdd == 'function') afterAdd.call(this); // кладем сюда пост-обработчики для созданного объекта
		
		//console.log('modalAdd', {'this': this, msg, tmp_obj, 'form': this.form, __content});
		//return {err: 'test', debugSave: true};
		return OK();
	},
};