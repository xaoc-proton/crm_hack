
exports.config = {
	multi: true,
	customName: true,
}

exports['modal'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{
	
		if(!c.add.items) c.add.items = [];
	
	return [

		["div",{"class": "modal fade tpl-theme-modal","role": "dialog","id": c.col+"-modal"},[
			c.add.modal.html ? c.add.modal.html(_, d) : [
				_.if(_.editMode, ()=>[
					_.c({name: 'tmp_obj_'+c.col, col: 'tmp_obj', config: c, process: {
						tpl: (_, d)=>{
						
							if(_.__.pre){
								Object.keys(_.config.add||[]).forEach((key)=>{
									if(typeof _.config.add[key] == 'function'){
										_.__.process[_.linecode+'_'+key] = {action: _.config.add[key]};
									}
								});
							}
						
							_.config.add.items.filter(_=>_).forEach((item, key)=>{
								if(_.__.pre){
									if(item.html){
										_.__.process[_.linecode+'_add_item_'+key] = {tpl: item.html};
									}
								}else{
									if(_.__.process[_.linecode+'_add_item_'+key]) item.html = _.__.process[_.linecode+'_add_item_'+key].tpl;
								}
							});
							
						return [
							
							["div",{"class": "modal-dialog","role": "document"},[
								
								["div",{"class": "modal-content"},[
									
									["div",{"class": "modal-header"},[
										["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
											["span",{"aria-hidden": "true"},[
												["i",{"class": "fas fa-times"}]
											]]
										]],
										["h4",{"class": "modal-title"},[
											["span", {"text": "Новый "+(_.config.label||'')}],
										]]
									]],
									
									["div",{"class": "modal-body"},[
										["div",{"role": "form", class: 'm-t'},[
										
											_.config.add.items.filter(_=>_).map(item=>[]
												.concat(item.f ? [_.f( item.f )] : [])
												.concat(item.html ? item.html(_, d) : [])
											),
											
										]],
									]],
									
									["div",{"class": "modal-footer"},[
										["button",{"type": "button","class": "btn btn-default","data-dismiss": "modal"},[
											["span",{"text": "Отменить"}]
										]],
										["button",{"type": "button","class": "btn btn-primary"},[
											["span",{"text": "Создать"}],
											_.f({name: '__tpl~modalAdd', type: 'action', process: (()=>{
												var obj = {};
												(SYS.get(_, 'config.add.required')||[]).forEach((item, key)=>{
													if(typeof item == 'function') obj['required_'+key] = item;
												});
												return obj;
											})(),front: {
												onAction: ()=>location.reload(),
												onError: ($e)=>reloadComplex($e),
											}}),
										]]
									]],
								]],
							]],
						]},
					}}),
				]),
			],	
		]],
		
	]},
	script: ()=>{
		$('.tpl-theme-modal').on('shown.bs.modal', function(e){
			$(e.target).addClass('show');
			if(window.resizeAllTextarea) window.resizeAllTextarea($(e.target));
		});
	},
	 style: ()=>{/*
		.tpl-theme-modal {
			position: fixed!important;
		}
		.tpl-theme-modal .el > .select2 {
			min-width: 100%;
		}
	*/},
}