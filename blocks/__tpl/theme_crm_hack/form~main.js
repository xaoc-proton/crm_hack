
exports.id = (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.fields[code].link = ['__'+__.fields[code].col];
	__.queryIds[code] = [__.user.key];
	callback();
}

exports.tpl = (_, d)=>{

	var userLST = SYS.get(LST, 'user~roles.lst.'+_.__.user.role) || {};

	//_.__.global.baseForm = userLST.baseForm || 'ce~list';
	_.__.global.baseForm = 'deal~list';

return [

	['style', {src:'https://use.fontawesome.com/releases/v5.1.0/css/all.css', integrity:"sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt",crossorigin:"anonymous"}],
	['style', {src:'blocks/__tpl/theme_crm_hack/static/css/custom.css'}],

	['script', {src:'XAOC/notify/notify.min.js'}],

		["div",{class: "layout-wrapper"},[
			["div",{class: "layout-inner"},[
				
				xHTML('__tpl~sidebar', _, d),
				
				["div",{class: "layout-container toggle layout-static-width"},[
					["div",{class: "layout-navbar"},[
						["input",{type: "text",placeholder: "Поиск",class: "form-control",id: "__BVID__4"}]
					]],
					
					['div', {id: 'formContent', class: 'role-'+_.__.user.role+' wrapper wrapper-content animated fadeIn'}, [

						_.form({name: SYS.get(_.__, 'user.query.subform.form') || _.__.global.baseForm}),
						
					]],
				]]
			]]
		]],
	
	_.if(userLST.tutorial !== false, ()=>['div', {id: 'subFormTutorial'}, [
		_.form({name: 'tutorial~main', history: false }),
	]]),
	
]}

exports.func = ()=>{
	
	window.notify = function(msg = {}, $target = [])
	{
		if($.notify)
		{
			if($target.length){
				$target.notify(msg.err, Object.assign({position: $target.attr('notifyPosition')}, msg.notify || {className: 'error'}));
			}else{
				$.notify(msg.err, msg.notify || {className: 'error'});
			}
		}
	}
	
}

exports.script = ()=>{

	// нотификации
	if($.notify){
		$.notify.defaults( {style: 'help'} );
		$.notify.addStyle('help', {
			html: "<div><span data-notify-text/></div>",
			classes: {
				base: {
					"color": "#018da8",
					"padding": "6px",
					"font-size": "14px",
					"border": "1px solid",
					"background-color": "black",
					"background-image": "url(/XAOC/images/clear-black-back.png)",
					"cursor": "pointer",
					"text-align": "left",
					"z-index": "9999",
				},
			}
		});
	}
	
	// подсказка по правой кнопке
	$(document).on('contextmenu', '[help]', function(e){
		e.preventDefault();
		var help = $(this).attr('help');
		if(help[0] == '['){
			help = JSON.parse(help);
			if(help[0] == 'lst' && window.LST[help[1]]){
				help = window.LST[help[1]].filter(function(h){ return h.v == help[2]})[0].help;
			}
		}
		$(this).notify(help,  { position: $(this).attr('helpPosition') || "top", style: "help" });
	});
	
	// вот это я не помню откуда, но явно полезное...
	$(document).on('click', '.disabled-item', function(e){
		var $this = $(this);
		if($this.hasClass('active')){
			$this.removeClass('active');
			$this.parent().find('> [prepare=disabled]').attr('disabled', 'disabled');
		}else{
			$this.addClass('active');
			$this.parent().find('> [prepare=disabled]').removeAttr('disabled').focus();
		}
	});
	
	/*
	 *
	 *   INSPINIA - Responsive Admin Theme
	 *   version 2.7.1
	 *
	 */

	$(document).ready(function () {

		// Add body-small class if window less than 768px
		if ($(this).width() < 769) {
			$('body').addClass('body-small')
		} else {
			$('body').removeClass('body-small')
		}
		
		// Collapse ibox function
		$(document).off('click', '.collapse-link');
		$(document).on('click', '.collapse-link', function(){
			var ibox = $(this).closest('div.ibox');
			var button = $(this).find('i');
			var content = ibox.children('.ibox-content');
			content.slideToggle(200);
			button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
			ibox.toggleClass('').toggleClass('border-bottom');
			setTimeout(function () {
				ibox.resize();
				ibox.find('[id^=map-]').resize();
			}, 50);
		});

		// Close ibox function
		$('.close-link').on('click', function () {
			var content = $(this).closest('div.ibox');
			content.remove();
		});

		// Fullscreen ibox function
		$('.fullscreen-link').on('click', function () {
			var ibox = $(this).closest('div.ibox');
			var button = $(this).find('i');
			$('body').toggleClass('fullscreen-ibox-mode');
			button.toggleClass('fa-expand').toggleClass('fa-compress');
			ibox.toggleClass('fullscreen');
			setTimeout(function () {
				$(window).trigger('resize');
			}, 100);
		});

		// Close menu in canvas mode
		$('.close-canvas-menu').on('click', function () {
			$("body").toggleClass("mini-navbar");
			SmoothlyMenu();
		});

		// Run menu of canvas
		$('body.canvas-menu .sidebar-collapse').slimScroll({
			height: '100%',
			railOpacity: 0.9
		});

		// Open close right sidebar
		$('.right-sidebar-toggle').on('click', function () {
			$('#right-sidebar').toggleClass('sidebar-open');
		});

		// Initialize slimscroll for right sidebar
		$('.sidebar-container').slimScroll({
			height: '100%',
			railOpacity: 0.4,
			wheelStep: 10
		});

		// Open close small chat
		$('.open-small-chat').on('click', function () {
			$(this).children().toggleClass('fa-comments').toggleClass('fa-remove');
			$('.small-chat-box').toggleClass('active');
		});

		// Initialize slimscroll for small chat
		$('.small-chat-box .content').slimScroll({
			height: '234px',
			railOpacity: 0.4
		});

		// Small todo handler
		$('.check-link').on('click', function () {
			var button = $(this).find('i');
			var label = $(this).next('span');
			button.toggleClass('fa-check-square').toggleClass('fa-square-o');
			label.toggleClass('todo-completed');
			return false;
		});

		// Append config box / Only for demo purpose
		// Uncomment on server mode to enable XHR calls
		//$.get("skin-config.html", function (data) {
		//    if (!$('body').hasClass('no-skin-config'))
		//        $('body').append(data);
		//});

		// Minimalize menu
		$( document ).on('click', '.navbar-minimalize', function (event) {
			$("body").toggleClass("mini-navbar");
			$(window).trigger('resize');
			SmoothlyMenu();
		});

		// Tooltips demo
		$('.tooltip-demo').tooltip({
			selector: "[data-toggle=tooltip]",
			container: "body"
		});


		// Full height of sidebar
		function fix_height() {
			var heightWithoutNavbar = $("body > #wrapper").height() - 61;
			$(".sidebar-panel").css("min-height", heightWithoutNavbar + "px");

			var navbarheight = $('nav.navbar-default').height();
			var wrapperHeight = $('#page-wrapper').height();

			if (navbarheight > wrapperHeight) {
				$('#page-wrapper').css("min-height", navbarheight + "px");
			}

			if (navbarheight < wrapperHeight) {
				$('#page-wrapper').css("min-height", $(window).height() + "px");
			}

			if ($('body').hasClass('fixed-nav')) {
				if (navbarheight > wrapperHeight) {
					$('#page-wrapper').css("min-height", navbarheight + "px");
				} else {
					$('#page-wrapper').css("min-height", $(window).height() - 60 + "px");
				}
			}

		}

		fix_height();

		// Fixed Sidebar
		$(window).bind("load", function () {
			if ($("body").hasClass('fixed-sidebar')) {
				$('.sidebar-collapse').slimScroll({
					height: '100%',
					railOpacity: 0.9
				});
			}
		});

		// Move right sidebar top after scroll
		$(window).scroll(function () {
			if ($(window).scrollTop() > 0 && !$('body').hasClass('fixed-nav')) {
				$('#right-sidebar').addClass('sidebar-top');
			} else {
				$('#right-sidebar').removeClass('sidebar-top');
			}
		});

		$(window).bind("load resize scroll", function () {
			if (!$("body").hasClass('body-small')) {
				fix_height();
			}
		});

		$("[data-toggle=popover]")
			.popover();

		// Add slimscroll to element
		$('.full-height-scroll').slimscroll({
			height: '100%'
		})
	});


	// Minimalize menu when screen is less than 768px
	$(window).bind("resize", function () {
		if ($(this).width() < 769) {
			$('body').addClass('body-small')
		} else {
			$('body').removeClass('body-small')
		}
	});

	// Local Storage functions
	// Set proper body class and plugins based on user configuration
	$(document).ready(function () {
		if (localStorageSupport()) {

			var collapse = localStorage.getItem("collapse_menu");
			var fixedsidebar = localStorage.getItem("fixedsidebar");
			var fixednavbar = localStorage.getItem("fixednavbar");
			var boxedlayout = localStorage.getItem("boxedlayout");
			var fixedfooter = localStorage.getItem("fixedfooter");

			var body = $('body');

			if (fixedsidebar == 'on') {
				body.addClass('fixed-sidebar');
				$('.sidebar-collapse').slimScroll({
					height: '100%',
					railOpacity: 0.9
				});
			}

			if (collapse == 'on') {
				if (body.hasClass('fixed-sidebar')) {
					if (!body.hasClass('body-small')) {
						body.addClass('mini-navbar');
					}
				} else {
					if (!body.hasClass('body-small')) {
						body.addClass('mini-navbar');
					}

				}
			}

			if (fixednavbar == 'on') {
				$(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
				body.addClass('fixed-nav');
			}

			if (boxedlayout == 'on') {
				body.addClass('boxed-layout');
			}

			if (fixedfooter == 'on') {
				$(".footer").addClass('fixed');
			}
		}
	});

	// check if browser support HTML5 local storage
	function localStorageSupport() {
		return (('localStorage' in window) && window['localStorage'] !== null)
	}

	// For demo purpose - animation css script
	function animationHover(element, animation) {
		element = $(element);
		element.hover(
			function () {
				element.addClass('animated ' + animation);
			},
			function () {
				//wait for animation to finish before removing classes
				window.setTimeout(function () {
					element.removeClass('animated ' + animation);
				}, 2000);
			});
	}

	function SmoothlyMenu() {
		if (!$('body').hasClass('mini-navbar') || $('body').hasClass('body-small')) {
			// Hide menu in order to smoothly turn on when maximize menu
			$('#side-menu').hide();
			// For smoothly turn on menu
			setTimeout(
				function () {
					$('#side-menu').fadeIn(400);
				}, 200);
		} else if ($('body').hasClass('fixed-sidebar')) {
			$('#side-menu').hide();
			setTimeout(
				function () {
					$('#side-menu').fadeIn(400);
				}, 100);
		} else {
			// Remove all inline style from jquery fadeIn function to reset menu state
			$('#side-menu').removeAttr('style');
		}
	}

	// Dragable panels
	function WinMove() {
		var element = "[class*=col]";
		var handle = ".ibox-title";
		var connect = "[class*=col]";
		$(element).sortable(
			{
				handle: handle,
				connectWith: connect,
				tolerance: 'pointer',
				forcePlaceholderSize: true,
				opacity: 0.8
			})
			.disableSelection();
	}
	
}

exports.style = ()=>{/*
	.notifyjs-corner {
		z-index: 10000!important;
	}
	.select2-container {
		z-index: 100;
	}
	.ui-widget {
		z-index: 101;
	}
	body.modal-open .select2-container--open {
		z-index: 10100;
	}
	body.modal-open .ui-widget {
		z-index: 10100!important;
	}
	.modal-body {
		overflow-y: auto;
		height: 70%;
	}
	
	.notifyjs-help-base{
		-moz-box-shadow: 0 0 3px #999;
		-webkit-box-shadow: 0 0 3px #999;
		box-shadow: 0 0 3px #999;
		opacity: .9;
		-ms-filter: alpha(opacity=90);
		filter: alpha(opacity=90);
		position: relative;
		overflow: hidden;
		-moz-border-radius: 3px;
		-webkit-border-radius: 3px;
		border-radius: 3px;
		border: none !important;
		margin: 0 !important;
		background: rgb(26, 179, 148) !important;
		color: rgb(255, 255, 255) !important;
		padding: 15px 15px 15px 15px !important;
		width: 300px !important;
	} 
	.notifyjs-help-base.notifyjs-help-error{
		background: rgb(237, 85, 101) !important;
	}
	.notifyjs-help-base.notifyjs-help-info{
		background: rgb(35, 198, 200) !important;
	}
	.notifyjs-help-base.notifyjs-help-warn{
		background: rgb(248, 172, 89) !important;
	}

	.select2-container > .select2-dropdown.select2_obj .select2-results__option {
		display: flex;
	}
	.select2-container > .select2-dropdown.select2_obj .select2-results__option > img {
		height: 30px;
		margin-right: 6px;
	}
	.select2-container > .select2-dropdown.select2_obj .select2-results__option > div {
		font-size: 11px;
	}
	.select2-container > .select2-dropdown.select2_obj .select2-results__option > div > p {
		font-size: 12px;
		color: black;
		font-weight: 600;
	}

	[help] {
		cursor: help;
	}
	[help]:hover {
		opacity: 0.7;
	}

	.disabled-item {
		position:absolute;
		left:0;
		right:0;
		top:0;
		bottom:0;
		cursor: pointer;
		z-index: 100;
	}
	.disabled-item.active {
		left: 20%;
		right: 20%;
		top: calc(100% - 20px);
		background-color: #ccc;
		border-radius: 4px;
		color: white;
	}
	.disabled-item.active:before {
		content: 'СОХРАНИТЬ';
		display: flex;
		font-size: 10px;
		justify-content: center;
		line-height: 20px;
	}
	
	.complex-block.inline-complex {
		position: relative;
		display: flex;
		flex-wrap: wrap;
		padding-top: 10px;
	}
	.complex-block.inline-complex > .complex-controls {
		right: 15px!important;
		left: auto!important;
		color: #1c84c6;
		top: -20px!important;
		text-align: right!important;
	}	
	
*/}
