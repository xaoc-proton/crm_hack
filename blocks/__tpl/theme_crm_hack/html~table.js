
exports.tpl = (_, d, c)=>{

	if(!c.filter) c.filter = {};
	if(!c.filter.items) c.filter.items = [];
	c.filter.items = c.filter.items.filter(_=>_);
	if(c.filter.items.length && !c.filter.findBtn) c.filter.findBtn = {};
	
	if(c.add === true) c.add = {modal: true};
	if(c.add){
		if(!c.add.btn) c.add.btn = {};
	}
	
	if(!c.table) c.table = {};
	if(!c.table.filter) c.table.filter = -10;
	if(!c.table.items) c.table.items = [];
	c.table.items = c.table.items.filter(_=>_);
	if(!c.table.itemLink) c.table.itemLink = {};
	if(!c.table.itemLink.type) c.table.itemLink.type = 'row';
	if(!c.table.itemLink.link) c.table.itemLink.link = {form: c.col+'~main', container: 'formContent'};

return [
	
	!(c.add||{}).modal ? [] : _.html('__tpl~modal', _, d, c),
	
	["div",{class: "row mb-3"},[
			c.filter.items.map(item=>
				["div",{"class": item.class || "col-4"},[
					_.f( Object.assign(item.f||{}, {stype: 'filter'}) ),
				]]
			)
			.concat(!c.filter.findBtn ? [] : (c.filter.findBtn.html || [
				["div",{"class": c.filter.findBtn.class||"col-1"},[
					['div', {text: c.filter.findBtn.label||'Найти', class: 'btn btn-primary'}, [
						_.f({name: 'find_btn', type: 'action', front: {
							onClick: (_)=>{
								var filter = {};
								_.closest('.ibox').find('.filter-field').map(function(i,f){
									if(f.classList.contains("hasDatepicker")){
										filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
									}else{
										filter[f.name] = f.type == 'checkbox' ? f.checked : f.value;
									} 
								});
								filter.force = true;
								reloadComplex(_.closest('.ibox').find('tbody'), filter);
							}
						}}),
					]],
				]]
			]))
			.concat(!c.add ? [] : (c.add.btn.html || [
				_.if(_.editMode, ()=>[
					["div",{"class": c.add.btn.class || "text-right"},[
						["div",{id: 'btn-new-'+c.col, "class": "btn btn-primary","data-toggle": "modal","data-target": "#"+c.col+"-modal"},[
							["span",{"text": c.add.btn.label || "Новый "+(c.label||'')}]
						]]
					]],
				]),
			]))
	]],
	
	
/* ["div",{class: "main-table table-responsive"},[
									["table",{role: "table",class: "table b-table table-hover table-sm",id: "__BVID__7"},[
										["thead",{role: "rowgroup",class: ""},[
											["tr",{role: "row",class: ""},[
												["th",{role: "columnheader",scope: "col",class: ""}],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Название сделки"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Этап"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Компания"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Дата подписания договора"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Ответственный"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Статус сделки"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "WAR"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["span",{text: "Ожидаемая выручка с НДС"}]
												]]
											]]
										]],
										["thead",{role: "rowgroup",class: ""},[
											["tr",{role: "row",class: ""},[
												["th",{role: "columnheader",scope: "col",class: ""},[
													["div",{class: "custom-control custom-checkbox b-custom-control-sm"},[
														["input",{type: "checkbox",class: "custom-control-input",value: "true",id: "__BVID__22"}],
														["label",{class: "custom-control-label",for: "__BVID__22"}]
													]]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",class: "form-control form-control-sm",id: "__BVID__24"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",disabled: "disabled",class: "form-control form-control-sm",id: "__BVID__26"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["select",{class: "custom-select custom-select-sm",id: "__BVID__28"},[
														["option",{value: ""},[
															["span",{text: "Все сделки"}]
														]],
														["option",{value: "a"},[
															["span",{text: "Не все сделки"}]
														]]
													]]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",class: "form-control form-control-sm",id: "__BVID__30"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",class: "form-control form-control-sm",id: "__BVID__32"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",disabled: "disabled",class: "form-control form-control-sm",id: "__BVID__34"}]
												]],
												["th",{role: "columnheader",scope: "col",class: ""}],
												["th",{role: "columnheader",scope: "col",class: ""},[
													["input",{type: "text",class: "form-control form-control-sm",id: "__BVID__37"}]
												]]
											]]
										]],
										["tbody",{role: "rowgroup"},[
											["tr",{role: "row",class: ""},[
												["td",{role: "cell",class: ""},[
													["div",{class: "custom-control custom-checkbox b-custom-control-sm"},[
														["input",{type: "checkbox",class: "custom-control-input",value: "true",id: "__BVID__41"}],
														["label",{class: "custom-control-label",for: "__BVID__41"}]
													]]
												]],
												["td",{role: "cell",class: ""},[
													["button",{type: "button",class: "btn btn-link"},[
														["span",{text: "Цифровая экосистема"}]
													]]
												]],
												["td",{role: "cell",class: "text-nowrap"},[
													["span",{class: "badge badge-primary"},[
														["span",{text: "4"}]
													]],
													["span",{text: "Решение"}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "ООО Альянс Банк"}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "12.02.2021"}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "Иванов С. К."}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "Открыта"}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "Нет"}]
												]],
												["td",{role: "cell",class: ""},[
													["span",{text: "55 000 000 ₽"}]
												]]
											]]
										]]
									]]
								]] */
	
	
	["div",{class: "main-table table-responsive"},[
		["table",{id: c.col+'-table', "class": "table b-table table-hover table-sm"},[
			["thead",{},[
				["tr",{},[
					( !c.table.checkItems ? [] : [
						["td",{"class": "min"},[
						
													// ["div",{class: "custom-control custom-checkbox b-custom-control-sm"},[
														// ["input",{type: "checkbox",class: "custom-control-input",value: "true",id: "__BVID__41"}],
														// ["label",{class: "custom-control-label",for: "__BVID__41"}]
													// ]]
						
							_.f({name: 'checkAllItems', type: 'check', stype: 'filter', config: {content: false}, front: {
								onFilter: ($e)=>{
									$e.closest('table')
										.find('[name=checkItem]')
										.prop('checked', $e.prop('checked')?'checked':'')
										.trigger('change');
								}
							}}),
						]]
					]).concat(c.table.items
						.concat(c.table.itemLink.type != 'col' ? [] : {label: '', class: 'min'})
						.map(item=>
							["td",{class: item.class||''},[
								["span",{"text": item.label||""}],
							]],
						)
					)
				]]
			]],
			_.html('__tpl~table_body', _, d, Object.assign(c, {
				row: {content: (_, d)=>[
					_.config.table.prepareItem(_, d, 
						
						["tr", _.config.table.itemLink.type == 'row'  ? {class: 'h', query: _.config.table.itemLink.query} : {}, [
							
							( !_.config.table.checkItems ? [] : [
								["td",{itemId: d._id},[
									_.f({name: 'checkItem', type: 'check', stype: 'filter', config: {content: false}}),
								]]
							]).concat(
								_.config.table.items.map(item=>["td",{class: item.class||''},[]
									
									.concat(item.f ? [_.f( item.f )] : [])
									
									.concat(item.c ? [_.c( {name: item.c.name, add: false, config: {f: item.c.f}, process: {
										tpl: (_, d)=>(_.config.f ? [ _.f( _.config.f ) ] : [])
									}})] : [])
									
									.concat(item.html ? item.html(_, d) : [])
									
								])
							).concat(
								[_.config.table.itemLink.type != 'col' ? [] : ["td", {class: 'edit h', query: _.config.table.itemLink.query}]]
							),
						]],
					)
				]}
			})),
		]]
	]],
	["hr"],
	["div",{"class": "row m-top-lg form-inline"},[
		
	]]
	
]};

exports.script = ()=>{

}

exports.style = ()=>{/*
	.table .max100 { max-width: 100px }
	.table .max150 { max-width: 150px }
	.table .max200 { max-width: 200px }
*/}
