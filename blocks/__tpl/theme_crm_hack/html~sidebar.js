exports.tpl = (_, d)=>[

	["nav",{id: "side-menu", class: "sidenav layout-sidenav toggle"},[
		["ul",{class: "nav flex-column"},[
			["li",{class: "navbar-text"},[
				["img",{src: "http://oktopus.pro/static/img/okt_logo_main.png",alt: "",class: "img-fluid"}]
			]],
			
			((_, d) => {
				var admin = SYS.get(_.__.user, 'access.admin');
				return Object.keys(BLOCK.form)
					.filter(key => BLOCK.form[key].menu)
					.filter(key => SYS.get(_.__.user, 'access.'+key.split('~')[0]) || admin )
					.map(key => {
						const item = BLOCK.form[key].menu;
						const form = key.replace('.js','');
						return ["li",{class: "nav-item mb-3"+`css
							position: relative;
						`,form: form},[
							["a",{"href": "#", query: '{"form":"'+form+'", "container":"formContent"}',class: "nav-link"},[
								["svg",{viewBox: "0 0 16 16",width: "1em",height: "1em",focusable: "false",role: "img","aria-label": "check2",xmlns: "http://www.w3.org/2000/svg",fill: "currentColor",class: "bi-check2 b-icon bi"},[
									["g",{},[
										["path",{d: "M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z"}]
									]]
								]],
								["span",{"text": item.label || key}]
							]],
						]]
					})
			})(_, d),
		]]
	]],

	[]||["nav",{"class": "navbar-default navbar-static-side","role": "navigation"},[
		
		["div",{"class": "sidebar-collapse"},[

			_.script(()=>{
				window.afterAllLoaded.push(function(){ $('#side-menu').metisMenu() });
			}),
			
			["ul",{"id": "side-menu", "class": "nav metismenu"+`css
				.*css* > li.nav-header {
					cursor: pointer;
				}
			`},[
				
				["li",{class: "nav-header", "href": "#", query: '{"form":"'+_.__.global.baseForm+'", "container":"formContent"}'},[
					["div",{class: "dropdown profile-element"},[
						["span",{},[
							["img",{"alt": "image","src": "/static/img/logo-white.png"}]
						]]
					]],
					["div",{class: "logo-element"},[
						["span",{text: (PROJECT+'').toUpperCase()}]
					]]
				]],
				
				((_, d) => {
					var admin = SYS.get(_.__.user, 'access.admin');
					return Object.keys(BLOCK.form)
						.filter(key => BLOCK.form[key].menu)
						.filter(key => SYS.get(_.__.user, 'access.'+key.split('~')[0]) || admin )
						.map(key => {
							const item = BLOCK.form[key].menu;
							const form = key.replace('.js','');
							return ["li",{form: form},[
								["a",{"href": "#", query: '{"form":"'+form+'", "container":"formContent"}'},[
									!item.icon ? [] : ["i",{"class": item.icon}],
									["span",{"class": "nav-label"},[
										["span",{"text": item.label || key}]
									]]
								]],
							]]
						})
				})(_, d),

			]]
		]]
	]]
]

exports.func = ()=>{
	
	window.breadcrumb = [];
	
	window.updateMenu = function (data) {
		if (
			data.req.action == "link" && data.req.history !== false && data.req.history !== "false"
		) {
			var info = {};

			var $p = $("#page-wrapper > .page-heading");
			var $b = $p.find(".breadcrumb");

			if (typeof window.updateMenuCustom == "function") {
				
				var l = window.breadcrumb.length, q = JSON.stringify(history.state.subform);
				
				if(l > 0 && window.breadcrumb.filter(_=>JSON.stringify(_.query)==q).length == 0){
					delete window.breadcrumb[l-1].active;
				}else{
					window.breadcrumb = [];
				}
				
				window.updateMenuCustom(info);
				window.updateMenuCustom = undefined;
			} else {
				window.breadcrumb = [];
			}

			$b.html("");
			window.breadcrumb.forEach(function (b) {
				if (b.active) {
					$b.append(
						$("<li />", { class: "active" }).append(
							$("<strong />", { text: b.text })
						)
					);
				} else {
					$b.append(
						$("<li />", {}).append(
							$("<a />", { query: JSON.stringify(b.query), text: b.text })
						)
					);
				}
				if (!info.form && b.query && b.query.form) info.form = b.query.form;
				if (!info.text && b.text) info.text = b.text;
			});

			if (!info.form) info.form = data.req.query.form;

			let $menu = $("#side-menu");

			$menu.find("li.active").removeClass("active");
			$menu.find("ul.in").removeClass("in");

			$menu.find("li > a").each(function () {
				let $item = $(this);
				let $li = $item.closest("li");
				if (
					($item.attr("query") || "").indexOf('"form":"' + info.form + '"') != -1
				) {
					$li.addClass("active");
					if ($li.parent().hasClass("collapse")) {
						$li.parent().addClass("in");
						$li.parent().closest("li").addClass("active");
					}
					let $label = $item.find("> .nav-label > span");
					info.text =
						($label.length ? $label.text() : $item.text()) || info.text;
				}
			});

			$p.find("h2").text(info.text);
		}
	}
}