
exports.config = {
	multi: true,
	customName: true,
}

exports['list'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{ return [
		["div",{class: "card"},[
			["div",{class: "card-body"},[
				c.content(_, d),
			]],
		]],
	]},
	script: ()=>{
		
	},
	style: ()=>{/*
	*/},
}