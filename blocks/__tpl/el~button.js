
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['button'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		if(data.label) data.text = data.label;
	
	return [
		['button', data],
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			
			if(!data.front) data.front = {};
			
			if(data.front.onClick) doAfterLoad.push((e,d)=>{
				
				realParent.find('button[code='+data.code+']').off('click').on('click', function(e){
					// .closest('button') на случай, если внутри кнопки есть дочерние элементы
					window[data.front.onClick]( $(e.target).closest('button') );
				});
				
			});
		},
	},
}

exports['button+'] =
exports['button-'] =
exports['button--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_button'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_button'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}