
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['datetime'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){

		data.type = 'datetime+'; // принудительно подключаем дефолтный датапикер браузера
		if(data.value) data.value = moment(new Date(data.value)).format('DD.MM.YYYY HH:mm');
	
	return [

		['div',{class: data.class+" form-group"},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['div', {class: 'input-group'}, [
				['input', Object.assign({}, data, {class: 'form-control', autocomplete: 'off'})],
				['span', {class: "input-group-addon", onclick: '$(this).closest(".input-group").find("input").datepicker("show")'}, [
					['i', {class: "fa fa-calendar"}],
				]],
			]],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			this.save = function($e, data, callback){
				data.value = moment(data.value, 'DD.MM.YYYY HH:mm').unix()*1000;
				callback( data.value ? true : {err: 'Дата не распознана'}, data );
			}
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}

			($.fn.datetimepicker ? doAfterLoad : afterAllLoaded).push(function(){
				
				var $e = realParent.find('input[code='+data.code+']');

				if($e.datetimepicker) $e.datetimepicker({dateFormat: "dd.mm.yy", timeFormat: 'HH:mm'});
				
			});
		}
	},
	script: ()=>{
		
		if($.datepicker) $.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );
		
	},
}

exports['datetime+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_datetime'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_datetime'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['datetime-'] = 
exports['datetime--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		if(data.value) data.value = moment(new Date(data.value)).format('LLL');
	
	return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}