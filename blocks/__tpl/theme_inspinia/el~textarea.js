
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['textarea'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div',{class: data.class+" form-group"},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['textarea', Object.assign({}, data, {
				text: data.value+'',
				class: 'form-control',
			})],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_textarea'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['textarea+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_textarea'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_textarea'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['textarea-'] = 
exports['textarea--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div',{class: data.class+" form-group"},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['pre', {text: data.value, style: 'background: none;'}],
		]],
		
	]},
}