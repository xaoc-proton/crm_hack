
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['radio'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div', {class: "radio-el "+data.class+" form-group", code: data.code}, [
			
			['label',{},[
				['span',{text: data.label}],
			]],
			['div', {class: 'elParent'}, [
				this.element = function(e){ return [
					['span', {}, [
						['input', {type: 'radio', stype: data.stype, name: data.name+'_'+data.code, id: data.code+'_'+e.v, checked: e.v == data.value, value: e.v, code: data.code}],
						['label', {text: e.l, for: data.code+'_'+e.v}],
					]]
				]},
			]],
		]],

	]},
	style: ()=>{/*
		.radio-el {
			display: flex;
		}
		.radio-el > label {
			padding-right: 20px;
		}
		.radio-el input[type=radio] {
			display: none;
		}
		.radio-el input + label {
			cursor: pointer;
			opacity: 0.3;
			margin-right: 10px;
		}
		.radio-el input:checked + label {
			opacity: 1;
		}
	*/},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_radio'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['radio+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_radio'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_radio'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['radio-'] = 
exports['radio--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}