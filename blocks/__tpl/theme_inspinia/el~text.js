
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['text'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		["div", {class: data.class+" form-group"},[
			["label", {}, [
				["span", {text: data.label}],
			]],
			['div', {text: typeof data.value == 'object' ? data.value.l : data.value, class: 'el-value' }],
		]],

	]},
}

exports['text+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_input'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_input'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['text-'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_text'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}

exports['text--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_text'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}