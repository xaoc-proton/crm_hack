
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['file'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		if(!data.value) data.value = {};

	return [

		['div', Object.assign({}, data, {class: "file-el "+data.class}),[
			['label',{},[
				['span', {text: data.label}],
				["div", {class: "btn btn-link edit-btn "+(data.addLabel?"with-text":"")}, !data.addLabel ? [] : [
					['span', {text: '\xa0'+data.addLabel}]
				]],
				data.delete === false ? [] : ["div", {class: "btn btn-link delete-btn"}],
			]],
			['a', {target: '_blank', href: data.value.l, text: data.value.n}],
			['input', Object.assign(data.img||{}, {type: 'file', multiple: true, style: 'display: none;'})],
		]],

	]},
	style: ()=>{/*css
		.file-el {
			position: relative;
		}
		.file-el > label {
			display: flex;
		}
		.file-el > label > span {
			width: 100%;
		}
		.file-el > label > .edit-btn {
			//position: absolute;
			top: 0px;
			padding: 0px;
			right: 0px;
			font-size: 90%;
		}
		.file-el > label > .btn-link {
			height: 15px;
			min-width: 15px;
			padding: 0px;
			background-color: white;
			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
		}
		.file-el > label > .btn-link.edit-btn:not(.with-text) {
			background-image: url(/XAOC/images/edit.png);
		}
		.file-el > label > .btn-link.delete-btn {
			background-image: url(/XAOC/images/delete.png);
		}
	*/},
	script: ()=>{

		$(document).off("click", ".el.file-el > label > .edit-btn");
		$(document).on("click", ".el.file-el > label > .edit-btn", function (e) {
			$(this).closest("label").parent().find("input[type=file]").trigger("click");
		});

		$(document).off("click", ".el.file-el > label > .delete-btn");
		$(document).on("click", ".el.file-el > label > .delete-btn", function (e) {
			
			if (!confirm("Подтвердите удаление")) return false;

			var $el = $(this).closest(".el");
			wsSendCallback(
				{ action: "save", code: $el.attr("code"), value: {} },
				function () {
					$el.find("a").attr("href", "").html("");
					$el
						.closest(".el:not(.file-el)")
						.find("div[type=img][code=" + $el.attr("code") + "]")
						.css("background-image", "none");
				},
				function () {
					$.notify("Ошибка удаления файла");
				}
			);
		});

		$(document).off("change", ".el:not(.control-el) input[type=file]");
		$(document).on("change", ".el:not(.control-el) input[type=file]", function (e) {
			var $input = $(this);
			uploadFile(e.currentTarget.files[0], $input.closest(".el"), function(){
				$input.val('');
			});
		});

		function uploadFile(file, $e, callback) {
			
			var postForm = new FormData();
			postForm.append("file", file);
			postForm.append("session", sessionStorage["xaoc_" + PROJECT + "_session"]);
			postForm.append("code", $e.attr("code"));

			var xhr = new XMLHttpRequest();
			xhr.onload = xhr.onerror = function () {
				if (this.status == 200) {
					var response = JSON.parse(this.response);
					if (response && response.value) {
						$e.find("a").attr("href", response.value.l).html(response.value.n);

						// обновление img, если file добавлен как инструмент обновления
						$e.parent()
							.find("> div[type=img][code=" + $e.attr("code") + "]")
							.css("background-image", "url(" + response.value.l + ")");

						$.notify("Файл сохранен", { className: "success" });

						if (callback) callback();
					}
				} else {
					console.log("error " + this.status);
					$.notify("Ошибка загрузки файла", { className: "error" });
				}
			};

			xhr.open("POST", "/upload/file/", true);
			xhr.send(postForm);
		}
	},
}

exports['file+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
		window.el['__tpl~el_file'].tpl.bind(this)(_, d, data, tpl),
	]},
}

exports['file-'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
		
		if(!data.value) data.value = {};
	
	return [

		['div', Object.assign({}, data, {style: 'position: relative;'}),[
			['label',{style: 'width: 100%; font-weight: normal; margin: 0px;'},[
				['span', {text: data.label}],
			]],
			['a', {target: '_blank', href: data.value.l, text: data.value.n}],
		]],

	]},
}

exports['file--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
		window.el['__tpl~el_file-'].tpl.bind(this)(_, d, data, tpl),
	]},
}

exports.action = {
	customLocal: true,
	info: 'Роутер для загрузки файлов (pid='+process.pid+')',
	f: function(conn, data, callback){try{
		
		if(typeof APP == 'undefined'){
			callback();
		}else
		{
			APP.all('/upload/file/', function(req, res, next)
			{
				if(req.busboy)
				{
					req.pipe(req.busboy);
									
					// если не загружаются файлы через complex-add: 'file', то скорее всего дело в отсутствии элемента с типом img* (кусок кода с if(process && process.length){...)
					
					var path = require('path');
					var fileData = {};
					
					req.busboy.on('field', (fieldname, val)=>{
						fileData[fieldname] = val;
					});

					req.busboy.on('file', (fieldname, file, filename)=>{try{
						
						fileData.value = path.basename(filename);
						if(path.extname(fileData.value) == '.js') fileData.value += '.trash';

						var fstream = fs.createWriteStream(PROJECT_LINK+'/upload/tmp/'+fileData.value); 
						file.pipe(fstream);
						
						fstream.on('close', ()=>{
							
							DB.session.get(fileData.session, '__', (err, session)=>{

								if(session && session.user){
									
									if(fileData.code){

										// формально тут нужны проверки из saveField
										DB.session.getParent({user: {
											key: session.user,
											session: fileData.session
										}}, fileData.code, session, ()=>{

											if(session.field && session.parent){
												
												var link = DB.getUploadLink(session.field.privy, session.parent.col, fileData.value, fileData.code);

												fs.rename(PROJECT_LINK+'upload/tmp/'+fileData.value, PROJECT_LINK+link, ()=>{
												
													DB.getMongo(db=>{
														
														var $set = {};
														$set[session.field.name] = {
															l: link,
															n: fileData.value,
														}
														
														db.collection(session.parent.col)
														.update({_id: ObjectId(session.parent._id)}, {$set: $set}, ()=>{
														
															res.send({value: $set[session.field.name]});
														
														});
													});
												});
											}else{
												res.status(500).send('Не найдено поле для сохранения файла');
											}
										});
									}else{
										res.status(500).send('Не указано поле для сохранения файла');
									}
								}else{
									res.status(500).send('Сессия не найдена');
								}
							});
						});
					}catch(e){
						console.log(e);
						res.status(500).send('Ошибка при сохранении файла на сервер');
					}});
					
				}else{
					res.status(500).send('Файлы не переданы');
				}
			});
			
			callback();
		}

	}catch(e){ console.log(e); callback({status: 'err', err: 'Ошибка'}) }}
}