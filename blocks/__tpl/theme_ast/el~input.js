
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['input'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
	
		["div",{class: data.class+" sh-input-wrapper"},[
			["fieldset",{class: "form-group is-valid"},[
				["legend",{tabindex: "-1",class: "bv-no-focus-ring col-form-label pt-0"},[
					["span",{text: data.label}]
				]],
				["div",{tabindex: "-1",role: "group",class: "bv-no-focus-ring"},[
					["div",{class: "input-slot"},[
						["input", Object.assign({}, data, {type: "text",class: "el-value form-control is-valid",label: data.label})]
					]],
					["div",{class: "form-control-description"},[
						["span",{text: "\xa0"}]
					]],
					["small",{tabindex: "-1",class: "form-text text-muted"},[
						["div",{class: "description"}]
					]]
				]],
			]],
		]],
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_input'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['input+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_input'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_ast~el_input'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['input-'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}

exports['input--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}