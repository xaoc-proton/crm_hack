
exports.config = {
	multi: true,
	customName: true,
}

exports['modal'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{
	
		if(!c.add.items) c.add.items = [];
	
	return [

		["div",{class: ''+`css
			.*css* {
				display: none;
				width: 100%;
				position: absolute;
				z-index: 1040;
			}
			.*css* > .modal {
				display: block;
			}
		`},[
			["div",{id: c.col+"-modal", role: "dialog", class: "modal fade show","aria-modal": true},[
		
		//["div",{"class": "modal fade tpl-theme-modal","role": "dialog","id": c.col+"-modal"},[
			c.add.modal.html ? c.add.modal.html(_, d) : [
				_.if(_.editMode, ()=>[
					_.c({name: 'tmp_obj_'+c.col, col: 'tmp_obj', config: c, process: {
						tpl: (_, d)=>{
						
							if(_.__.pre){
								Object.keys(_.config.add||[]).forEach((key)=>{
									if(typeof _.config.add[key] == 'function'){
										_.__.process[_.linecode+'_'+key] = {action: _.config.add[key]};
									}
								});
							}
						
							_.config.add.items.filter(_=>_).forEach((item, key)=>{
								if(_.__.pre){
									if(item.html){
										_.__.process[_.linecode+'_add_item_'+key] = {tpl: item.html};
									}
								}else{
									if(_.__.process[_.linecode+'_add_item_'+key]) item.html = _.__.process[_.linecode+'_add_item_'+key].tpl;
								}
							});
							
						return [
							
							["div",{class: "modal-dialog modal-lg"},[
								
								["div",{class: "modal-content", tabindex: "-1"},[
								
									["header",{class: "modal-header"},[
										["h2",{class: "modal-title"},[
											["span",{text: "Новый "+(_.config.label||'')}]
										]],
										["button",{type: "button", "aria-label": "Закрыть", class: "close"},[
											["span",{text: "×"}]
										]]
									]],
									
									["div",{class: "modal-body"},[
										["span",{},[
											_.config.add.items.filter(_=>_).map(item=>
												["div",{class: "row"},[]
													.concat(item.f ? [_.f( item.f )] : [])
													.concat(item.html ? item.html(_, d) : [])
												]
											),
											['hr'],
											["div",{class: "row align-items-start"},[
												["div",{class: "col-auto ml-auto"},[
													["button",{type: "button",class: "btn btn-primary"},[
														["span",{"text": "Создать"}],
														_.f({name: '__tpl~modalAdd', type: 'action', process: (()=>{
															var obj = {};
															(SYS.get(_, 'config.add.required')||[]).forEach((item, key)=>{
																if(typeof item == 'function') obj['required_'+key] = item;
															});
															return obj;
														})(),front: {
															onAction: ()=>location.reload(),
															onError: ($e)=>reloadComplex($e),
														}}),
													]]
												]]
											]],
										]],
									]],
								]],
							]],
						]},
					}}),
				]),
			],	
		//]],
			]],
			["div",{class: "modal-backdrop"}],
		]],
	]},
	script: ()=>{
		/*$('.modal').on('shown.bs.modal', function(e){
			if(window.resizeAllTextarea) window.resizeAllTextarea($(e.target));
		});*/
		$(document).off('click', '[toggle-modal]').on('click', '[toggle-modal]', function(){
			const $this = $(this);
			const $modal = $($this.attr('toggle-modal')).parent();
			console.log('$modal', $modal);
			$modal.show();
			if(window.resizeAllTextarea) window.resizeAllTextarea($(e.target));
		});
		$(document).off('click', '.modal-backdrop').on('click', '.modal-backdrop', function(){
			$(this).parent().hide();
		});
		$(document).off('click', '.modal-header > .close').on('click', '.modal-header > .close', function(){
			$(this).closest('.modal').parent().hide();
		});
	},
	 style: ()=>{/*
		.tpl-theme-modal {
			position: fixed!important;
		}
		.tpl-theme-modal .el > .select2 {
			min-width: 100%;
		}
	*/},
}