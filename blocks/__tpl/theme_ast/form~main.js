
exports.id = (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.fields[code].link = ['__'+__.fields[code].col];
	__.queryIds[code] = [__.user.key];
	callback();
}

exports.tpl = (_, d)=>{

	var userLST = SYS.get(LST, 'user~roles.lst.'+_.__.user.role) || {};

	_.__.global.baseForm = userLST.baseForm || 'ce~list';

return [

	['style', {src:'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;display=swap&amp;subset=cyrillic'}],
	['style', {src:'blocks/__tpl/theme_ast/static/css/vendors.app.css'}],
	['style', {src:'blocks/__tpl/theme_ast/static/css/app.css'}],
	['style', {src:'blocks/__tpl/theme_ast/static/css/lab.css'}],
	
	['script', {src:'blocks/__tpl/theme_ast/static/js/runtime.js'}],
	['script', {src:'blocks/__tpl/theme_ast/static/js/commons.app.js'}],
	['script', {src:'blocks/__tpl/theme_ast/static/js/vendors.app.js'}],
	['script', {src:'blocks/__tpl/theme_ast/static/js/app.js'}],
	['script', {src:'XAOC/notify/notify.min.js'}],
	
	["div",{id: "__nuxt"},[
		["div",{id: "__layout"},[
			["div",{class: "layout-wrapper"},[
				["div",{class: "layout-inner"},[
					
					_.html('__tpl~sidebar', _, {}),
					
					["div",{class: "layout-container"},[
						
						_.html('__tpl~navbar', _, {}),
						
						['div', {id: 'formContent', class: 'layout-content role-'+_.__.user.role}, [
							_.form({name: SYS.get(_.__, 'user.query.subform.form') || _.__.global.baseForm}),
						]],
					]],
				]],
				
				["div",{class: "layout-right-bar"},[
					["div",{class: "vue-portal-target"}],
					["button",{type: "button",class: "btn layout-right-bar__close btn-icon-default"},[
						["div",{class: "sh-icon sh-icon-close"},[
							["div",{class: "sh-icon-wraper",style: "width: 10px; height: 10px;"},[
								["svg",{width: "16",height: "16",viewBox: "0 0 16 16",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
									["path",{d: "M1.82091 0.312419C1.40435 -0.10414 0.728979 -0.10414 0.312419 0.312419C-0.10414 0.728979 -0.10414 1.40435 0.312419 1.82091L6.49151 8L0.31242 14.1791C-0.10414 14.5956 -0.10414 15.271 0.312419 15.6876C0.728979 16.1041 1.40435 16.1041 1.82091 15.6876L8 9.50849L14.1791 15.6876C14.5956 16.1041 15.271 16.1041 15.6876 15.6876C16.1041 15.271 16.1041 14.5956 15.6876 14.1791L9.50849 8L15.6876 1.82091C16.1041 1.40436 16.1041 0.728979 15.6876 0.31242C15.271 -0.10414 14.5956 -0.10414 14.1791 0.31242L8 6.49151L1.82091 0.312419Z",fill: "#949899",class: "fill"}]
								]]
							]]
						]]
					]]
				]],
				["div",{class: "bPopoverLayer"}],
				["div",{class: "vue-back-to-top",style: "bottom: 40px; right: 30px; display: none;"},[
					["svg",{width: "8",height: "14",viewBox: "0 0 8 14",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: "w-8"},[
						["path",{d: "M4.35355 0.646446C4.15829 0.451184 3.84171 0.451184 3.64645 0.646446L0.464467 3.82843C0.269204 4.02369 0.269204 4.34027 0.464467 4.53553C0.659729 4.7308 0.976311 4.7308 1.17157 4.53553L4 1.70711L6.82843 4.53553C7.02369 4.7308 7.34027 4.7308 7.53553 4.53553C7.7308 4.34027 7.7308 4.02369 7.53553 3.82843L4.35355 0.646446ZM4.5 14L4.5 1L3.5 1L3.5 14L4.5 14Z",class: "fill"}]
					]],
					// надо найти скрипт
					["span",{text: "наверх"}]
				]],
				["div",{class: "vue-notification-group",style: "width: 300px; top: 0px; right: 0px;"},[
					["span"]
				]]
			]],
		]],
	]],	
]}

exports.func = ()=>{
	
	window.notify = function(msg = {}, $target = [])
	{
		if($.notify)
		{
			if($target.length){
				$target.notify(msg.err, Object.assign({position: $target.attr('notifyPosition')}, msg.notify || {className: 'error'}));
			}else{
				$.notify(msg.err, msg.notify || {className: 'error'});
			}
		}
	}
	
}

exports.script = ()=>{
	
	$(document).off('click', '.tooltip-parent').on('click', '.tooltip-parent', function(){
		const $popover = $(this).find('> .popover');
		$popover.toggleClass('hidden');
		$('body').one('click', function(){ $popover.addClass('hidden') });
	});
}

exports.style = ()=>{/*
	html {
		font-size: 16px;
	}
	.label {
		color: #231f20;
	}
	.hidden {
		display: none!important;
	}
*/}